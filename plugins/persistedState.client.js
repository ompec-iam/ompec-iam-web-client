import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'
const ls = new SecureLS({ encodingType: 'aes', isCompression: false, encryptionSecret: 'my-secret-key' })

export default ({ store }) => {
  createPersistedState({
    key: 'appstorage',
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })(store)
}
