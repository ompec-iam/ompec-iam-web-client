import ru from '../locales/ru.json'
import en from '../locales/en.json'

export default {
  locale: 'ru',
  fallbackLocale: 'ru',
  messages: { ru, en }
}
