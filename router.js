import Vue from 'vue'
import Router from 'vue-router'

function interopDefault (promise) {
  return promise.then(m => m.default || m)
}

const SignInPage = () => interopDefault(import('~/pages/SignInPage.vue'))
const EmployeePage = () => interopDefault(import('~/pages/Employee.vue'))
const EmployeeInfoPage = () => interopDefault(import('~/pages/EmployeeInfo.vue'))
const Departments = () => interopDefault(import('~/pages/Departments.vue'))
const DepartmentInfo = () => interopDefault(import('~/pages/DepartmentInfo.vue'))
const Resources = () => interopDefault(import('~/pages/Resource.vue'))
const ResourceInfo = () => interopDefault(import('~/pages/ResourceInfo.vue'))
const PermissionStatus = () => interopDefault(import('~/pages/PermissionStatus.vue'))
const EmployeeStatus = () => interopDefault(import('~/pages/EmployeeStatus.vue'))
const EmployeeStatusInfo = () => interopDefault(import('/pages/EmployeeStatusInfo.vue'))
const PermissionStatusInfo = () => interopDefault(import('~/pages/PermissionStatusInfo.vue'))
const UserPage = () => interopDefault(import('~/pages/User.vue'))
const Search = () => interopDefault(import('~/pages/Search.vue'))

Vue.use(Router)

export function createRouter () {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/signin',
        component: SignInPage
      },
      {
        path: '/employee',
        component: EmployeePage
      },
      {
        path: '/employee/:id',
        component: EmployeeInfoPage
      },
      {
        path: '/department',
        component: Departments
      },
      {
        path: '/department/:id',
        component: DepartmentInfo
      },
      {
        path: '/resource',
        component: Resources
      },
      {
        path: '/resource/:id',
        component: ResourceInfo
      },
      {
        path: '/permission_status',
        component: PermissionStatus
      },
      {
        path: '/permission_status/:id',
        component: PermissionStatusInfo
      },
      {
        path: '/user',
        component: UserPage
      },
      {
        path: '/search',
        component: Search
      },
      {
        path: '/employee_status',
        component: EmployeeStatus
      },
      {
        path: '/employee_status/:id',
        component: EmployeeStatusInfo
      }
    ]
  })
}
