import ENV from '~/env.js'

const state = () => ({
  isLoadingDepartments: false,
  departments: [
  ],
  currentDepartmentEmployees: [],
  page: 1,
  numberOfPages: 0,
  totalEmployees: 0,
  isLoadingEmployees: true,
  currentDepartment: {
    id: '',
    title: '',
    employeeCount: ''
  }
})

const mutations = {
  SET_ID_LOADING_DEPARTMENT (state, isLoading) {
    state.isLoadingEmployees = isLoading
  },
  SET_DEPARTMENTS (state, departments) {
    state.departments = departments
  },
  SET_CURRENT_DEPARTMENT (state, department) {
    state.currentDepartment = department
  },
  SET_LOADING_DEPARTMENT_EMPLOYEES (state, isLoading) {
    state.isLoadingEmployees = isLoading
  },
  SET_CURRENT_DEPARTMENT_EMPLOYEES (state, employees) {
    state.currentDepartmentEmployees = employees
  },
  SET_CURRENT_DEPARTMENT_NUMBER_OF_PAGES (state, numberOfPages) {
    state.numberOfPages = numberOfPages
  },
  SET_CURRENT_DEPARTMENT_TOTAL_EMPLOYEES (state, numberOfEmployees) {
    state.totalEmployees = numberOfEmployees
  }
}

const actions = {
  loadDepartments ({ commit }) {
    commit('SET_ID_LOADING_DEPARTMENT', true)
    this.$axios.get(ENV.LOAD_DEPARTMENTS_API_URL, {
      params: {
        lang: this.$i18n.locale
      }
    }).then((res) => {
      commit('SET_ID_LOADING_DEPARTMENT', false)
      commit('SET_DEPARTMENTS', res.data.departments)
    })
  },
  fetchDepartment ({ commit }, departmentId) {
    this.$axios.get(ENV.FETCH_DEPARTMENT_BY_ID_API_URL + departmentId)
      .then((res) => {
        commit('SET_CURRENT_DEPARTMENT', res.data)
      })
  },
  createDepartment ({ commit }, request) {
    return this.$axios.post(ENV.CREATE_NEW_DEPARTMENT_API_URL, request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  updateDepartment ({ commit }, payload) {
    return this.$axios.put(ENV.UPDATE_DEPARTMENT_API_URL, payload, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  removeDepartments ({ commit }, payload) {
    const request = {
      departments: payload
    }
    return this.$axios.delete(ENV.REMOVE_DEPARTMENTS_API_URL, {
      params: {
        lang: this.$i18n.lang
      },
      data: request
    })
  },
  fetchEmployeesFromDepartments ({ commit }, payload) {
    commit('SET_LOADING_DEPARTMENT_EMPLOYEES', true)
    const { page, itemsPerPage, sortBy, sortDesc } = payload.options
    const currentPage = page - 1
    const rParams = new URLSearchParams()
    rParams.append('size', itemsPerPage)
    rParams.append('page', currentPage)
    rParams.append('departmentId', payload.departmentId)
    for (let i = 0; i < sortBy.length; i++) {
      const prop = sortBy[i]
      const sortDirection = sortDesc[i] ? 'desc' : 'asc'
      rParams.append('sort', `${prop},${sortDirection}`)
    }
    this.$axios.get(ENV.LOAD_EMPLOYEE_URL, {
      params: rParams
    })
      .then((res) => {
        commit('SET_LOADING_DEPARTMENT_EMPLOYEES', false)
        commit('SET_CURRENT_DEPARTMENT_EMPLOYEES', res.data.content)
        commit('SET_CURRENT_DEPARTMENT_TOTAL_EMPLOYEES', res.data.totalElements)
        commit('SET_CURRENT_DEPARTMENT_NUMBER_OF_PAGES', res.data.totalPages)
      })
  }
}

const getters = {
  getDepartments: state => state.departments,
  isLoadingDepartments: state => state.isLoadingDepartments,
  currentDepartment: state => state.currentDepartment,
  getCurrentDepartmentEmployees: state => state.currentDepartmentEmployees,
  getCurrentDepartmentEmployeesPage: state => state.page,
  getCurrentDepartmentNumberOfPages: state => state.numberOfPages,
  getCurrentDepartmentTotalEmployees: state => state.totalEmployees,
  getCurrentDepartmentEmployeesIsLoading: state => state.isLoadingEmployees
}

export default {
  state,
  mutations,
  actions,
  getters
}
