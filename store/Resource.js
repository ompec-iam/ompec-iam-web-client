import ENV from '~/env.js'

const state = () => ({
  isLoadingResources: false,
  resources: [],
  currentResource: {
    id: '',
    title: '',
    permissionCount: ''
  },
  currentResourcePermissions: [],
  page: 1,
  numberOfPages: 0,
  totalPermissions: 0,
  isLoadingPermissions: true
})

const mutations = {
  SET_IS_LOADING_RESOURCES (state, isLoading) {
    state.isLoadingResources = isLoading
  },
  SET_RESOURCES (state, resources) {
    state.resources = resources
  },
  SET_CURRENT_RESOURCE (state, resource) {
    state.currentResource = resource
  },
  SET_CURRENT_RESOURCE_PERMISSIONS (state, permissions) {
    state.currentResourcePermissions = permissions
  },
  SET_CURRENT_RESOURCE_NUMBER_OF_PAGES (state, num) {
    state.numberOfPages = num
  },
  SET_CURRENT_RESOURCE_TOTAL_PERMISSIONS (state, num) {
    state.totalPermissions = num
  },
  SET_CURRENT_RESOURCE_IS_LOADING_PERMISSION (state, isLoading) {
    state.isLoadingPermissions = isLoading
  }
}

const actions = {
  loadResources ({ commit }) {
    commit('SET_IS_LOADING_RESOURCES', true)
    this.$axios.get(ENV.LOAD_RESOURCES_API_URL, {
      params: {
        lang: this.$i18n.lang
      }
    }).then((res) => {
      commit('SET_IS_LOADING_RESOURCES', false)
      commit('SET_RESOURCES', res.data.resources)
    })
  },
  fetchResource ({ commit }, payload) {
    this.$axios.get(ENV.FETCH_RESOURCE_BY_ID_API_URL + payload.resourceId).then((res) => {
      commit('SET_CURRENT_RESOURCE', res.data)
    })
  },
  createResource ({ commit }, payload) {
    return this.$axios.post(ENV.CREATE_NEW_RESOURCE_API_URL, payload.data, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  deleteResources ({ commit }, payload) {
    return this.$axios.delete(ENV.REMOVE_RESOURCES_BY_ID_API_URL, {
      params: {
        lang: this.$i18n.land
      },
      data: payload
    })
  },
  updateResource ({ commit }, payload) {
    return this.$axios.put(ENV.UPDATE_RESOURCE_API_URL, payload.request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  fetchPermissionsFromResource ({ commit }, payload) {
    commit('SET_CURRENT_RESOURCE_IS_LOADING_PERMISSION', true)
    const { page, itemsPerPage, sortBy, sortDesc } = payload.options
    const currentPage = page - 1
    const rParams = new URLSearchParams()
    rParams.append('size', itemsPerPage)
    rParams.append('page', currentPage)
    for (let i = 0; i < sortBy.length; i++) {
      const prop = sortBy[i]
      const sortDirection = sortDesc[i] ? 'desc' : 'asc'
      rParams.append('sort', `${prop},${sortDirection}`)
    }
    rParams.append('filter', `resourceId:${payload.resourceId}`)
    this.$axios.get(ENV.LOAD_PERMISSIONS_API_URL, {
      params: rParams
    }).then((res) => {
      commit('SET_CURRENT_RESOURCE_IS_LOADING_PERMISSION', false)
      commit('SET_CURRENT_RESOURCE_PERMISSIONS', res.data.content)
      commit('SET_CURRENT_RESOURCE_TOTAL_PERMISSIONS', res.data.totalElements)
      commit('SET_CURRENT_RESOURCE_NUMBER_OF_PAGES', res.data.totalPages)
    })
  }
}

const getters = {
  getResources: state => state.resources,
  isLoadingResources: state => state.isLoadingResources,
  getCurrentResource: state => state.currentResource,
  getCurrentResourcePermissions: state => state.currentResourcePermissions,
  getCurrentResourcePage: state => state.page,
  getCurrentResourceNumberOfPages: state => state.numberOfPages,
  getCurrentResourceTotalPermissions: state => state.totalPermissions,
  getCurrentResourceIsLoadingPermissions: state => state.isLoadingPermissions
}

export default {
  state,
  mutations,
  actions,
  getters
}
