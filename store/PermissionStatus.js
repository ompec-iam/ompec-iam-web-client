import ENV from '~/env.js'

const state = () => ({
  isLoadingStatuses: false,
  statuses: [],
  currentPermissionStatus: {
    id: '',
    status: '',
    color: '',
    permissionCount: ''
  },
  currentPermissionStatusPermissions: [],
  page: 1,
  numberOfPages: 0,
  totalPermissions: 0,
  isLoadingPermissions: true
})

const mutations = {
  SET_IS_LOADING_PERMISSION_STATUSES (state, isLoading) {
    state.isLoadingStatuses = isLoading
  },
  SET_PERMISSION_STATUSES (state, statuses) {
    state.statuses = statuses
  },
  SET_CURRENT_PERMISSION_STATUS (state, status) {
    state.currentPermissionStatus = status
  },
  SET_CURRENT_PERMISSION_STATUS_PERMISSIONS (state, permissions) {
    state.currentPermissionStatusPermissions = permissions
  },
  SET_CURRENT_PERMISSION_STATE_NUMBER_OF_PAGES (state, num) {
    state.numberOfPages = num
  },
  SET_CURRENT_PERMISSION_STATUS_TOTAL_PERMISSIONS (state, num) {
    state.totalPermissions = num
  },
  SET_CURRENT_PERMISSION_STATUS_IS_LOADING_PERMISSIONS (state, isLoading) {
    state.isLoadingPermissions = isLoading
  }
}

const actions = {
  loadPermissionStatuses ({ commit }) {
    commit('SET_IS_LOADING_PERMISSION_STATUSES', true)
    this.$axios(ENV.LOAD_PERMISSION_STATUSES_API_URL, {
      params: {
        lang: this.$i18n.lang
      }
    }).then((res) => {
      commit('SET_IS_LOADING_PERMISSION_STATUSES', false)
      commit('SET_PERMISSION_STATUSES', res.data.statuses)
    })
  },
  createPermissionStatus ({ commit }, payload) {
    return this.$axios.post(ENV.CREATE_PERMISSION_STATUS_API_URL, payload.request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  fetchPermissionStatus ({ commit }, payload) {
    this.$axios.get(ENV.FETCH_PERMISSION_STATUS_BY_ID_API_URL + payload.statusId, {
      params: {
        lang: this.$i18n.lang
      }
    }).then((res) => {
      commit('SET_CURRENT_PERMISSION_STATUS', res.data)
    })
  },
  deletePermissionStatus ({ commit }, payload) {
    return this.$axios.delete(ENV.REMOVE_PERMISSION_STATUS_BY_ID_API_URL, {
      params: {
        lang: this.$i18n.lang
      },
      data: payload.request
    })
  },
  updatePermissionStatus ({ commit }, payload) {
    return this.$axios.put(ENV.UPDATE_PERMISSION_STATUS_BY_ID_API_URL, payload.request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  fetchPermissionFromPermissionStatus ({ commit }, payload) {
    commit('SET_CURRENT_PERMISSION_STATUS_IS_LOADING_PERMISSIONS', true)
    const { page, itemsPerPage, sortBy, sortDesc } = payload.options
    const currentPage = page - 1
    const rParams = new URLSearchParams()
    rParams.append('size', itemsPerPage)
    rParams.append('page', currentPage)
    for (let i = 0; i < sortBy.length; i++) {
      const prop = sortBy[i]
      const sortDirection = sortDesc[i] ? 'desc' : 'asc'
      rParams.append('sort', `${prop},${sortDirection}`)
    }
    rParams.append('filter', `statusId:${payload.statusId}`)
    this.$axios.get(ENV.LOAD_PERMISSIONS_API_URL, {
      params: rParams
    }).then((res) => {
      commit('SET_CURRENT_PERMISSION_STATUS_IS_LOADING_PERMISSIONS', false)
      commit('SET_CURRENT_PERMISSION_STATUS_PERMISSIONS', res.data.content)
      commit('SET_CURRENT_PERMISSION_STATUS_TOTAL_PERMISSIONS', res.data.totalElements)
      commit('SET_CURRENT_PERMISSION_STATE_NUMBER_OF_PAGES', res.data.totalPages)
    })
  }
}

const getters = {
  getPermissionStatuses: state => state.statuses,
  getPermissionStatusesIsLoading: state => state.isLoadingStatuses,
  getCurrentPermissionStatus: state => state.currentPermissionStatus,
  getCurrentPermissionStatusPermissions: state => state.currentPermissionStatusPermissions,
  getCurrentPermissionStatusPage: state => state.page,
  getCurrentPermissionStatusNumberOfPages: state => state.numberOfPages,
  getCurrentPermissionStatusTotalPermissions: state => state.totalPermissions,
  getCurrentPermissionStatusIsLoadingPermissions: state => state.isLoadingPermissions
}

export default {
  state,
  mutations,
  actions,
  getters
}
