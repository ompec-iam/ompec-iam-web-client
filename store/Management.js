const state = () => ({
})

const mutations = {
}

const actions = {
}

const getters = {
  currentUserIsAdmin: (state, getters, rootState) => {
    return rootState.auth.user.roles.map(r => r.name).includes('ROLE_ADMIN')
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
