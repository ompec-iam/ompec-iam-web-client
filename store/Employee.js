import ENV from '~/env.js'

const mockCurrentEmployee = {
  department: {
    title: ''
  }
}

const state = () => ({
  employees: [
  ],
  page: 1,
  numberOfPages: 0,
  totalEmployees: 0,
  isLoadingEmployees: true,
  // t
  currentEmployee: mockCurrentEmployee,
  currentEmployeeIsLoading: false
})

const mutations = {
  SET_LOADING_EMPLOYEES (state, isLoading) {
    state.isLoadingEmployees = isLoading
  },
  SET_CURRENT_EMPLOYEE (state, employee) {
    state.currentEmployee = employee
  },
  SET_EMPLOYEES (state, employees) {
    state.employees = employees
  },
  SET_TOTAL_EMPLOYEES (state, totalEmployees) {
    state.totalEmployees = totalEmployees
  },
  SET_NUMBER_OF_PAGES (state, num) {
    state.numberOfPages = num
  },
  ADD_EMPLOYEE (state, employee) {
    state.employees.push(employee)
  },
  LOADING_CURRENT_EMPLOYEE (state, isLoading) {
    state.currentEmployeeIsLoading = isLoading
  },
  REMOVE_EMPLOYEE (state, employeeId) {
    state.employees = [
      ...state.employees.filter(e => e.id !== employeeId)
    ]
  },
  SET_DEFAULT_CURRENT_EMPLOYEE (state) {
    state.currentEmployee = mockCurrentEmployee
  }
}

const actions = {
  fetchEmployeesFromApi ({ commit }, payload) {
    commit('SET_LOADING_EMPLOYEES', true)
    const { page, itemsPerPage, sortBy, sortDesc } = payload
    const currentPage = page - 1
    const rParams = new URLSearchParams()
    rParams.append('size', itemsPerPage)
    rParams.append('page', currentPage)
    for (let i = 0; i < sortBy.length; i++) {
      const prop = sortBy[i]
      const sortDirection = sortDesc[i] ? 'desc' : 'asc'
      rParams.append('sort', `${prop},${sortDirection}`)
    }
    this.$axios.get(ENV.LOAD_EMPLOYEE_URL, {
      params: rParams
    })
      .then((response) => {
        commit('SET_LOADING_EMPLOYEES', false)
        commit('SET_EMPLOYEES', response.data.content)
        commit('SET_TOTAL_EMPLOYEES', response.data.totalElements)
        commit('SET_NUMBER_OF_PAGES', response.data.totalPages)
      })
  },
  fetchEmployee ({ commit }, id) {
    commit('LOADING_CURRENT_EMPLOYEE', true)
    this.$axios.get(ENV.FETCH_EMPLOYEE_BY_ID + id, {
      params: {
        lang: this.$i18n.locale
      }
    }).then((response) => {
      commit('SET_CURRENT_EMPLOYEE', response.data)
      commit('LOADING_CURRENT_EMPLOYEE', false)
    }).catch((e) => {
      commit('LOADING_CURRENT_EMPLOYEE', false)
    })
  },
  addEmployee ({ commit }, employee) {
    return this.$axios.post(ENV.CREATE_NEW_EMPLOYEE_API_URL, employee, {
      params: {
        lang: this.$i18n.locale
      }
    })
  },
  updateEmployee ({ commit }, request) {
    return this.$axios.put(ENV.UPDATE_EMPLOYEE_API_URL, request, {
      params: {
        lang: this.$i18n.locale
      }
    })
  },
  removePermissions ({ commit }, request) {
    return this.$axios.delete(ENV.REMOVE_PERMISSIONS, {
      params: {
        lang: this.$i18n.locale
      },
      data: request
    })
  },
  removeEmployee ({ commit }, employeeId) {
    this.$axios.delete(ENV.REMOVE_EMPLOYEE_API + employeeId, {
      params: {
        lang: this.$i18n.locale
      }
    }).then((res) => {
      commit('REMOVE_EMPLOYEE', employeeId)
      commit('SET_DEFAULT_CURRENT_EMPLOYEE')
    })
  },
  removeEmployees ({ commit }, payload) {
    const request = {
      employees: payload
    }
    return this.$axios.delete(ENV.REMOVE_EMPLOYEE_API, {
      params: {
        lang: this.$i18n.locale
      },
      data: request
    })
  },
  createPermission ({ commit }, payload) {
    return this.$axios.post(ENV.CREATE_NEW_PERMISSION_API_URL, payload.request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  },
  updatePermission ({ commit }, payload) {
    return this.$axios.put(ENV.UPDATE_PERMISSION_BY_ID_API_URL, payload.request, {
      params: {
        lang: this.$i18n.lang
      }
    })
  }
}

const getters = {
  getEmployees: state => state.employees,
  getEmployeePage: state => state.page,
  getEmployeeNumberOfPages: state => state.numberOfPages,
  getTotalEmployees: state => state.totalEmployees,
  isLoadingEmployees: state => state.isLoadingEmployees,
  currentEmployee: state => state.currentEmployee,
  currentEmployeeIsLoading: state => state.currentEmployeeIsLoading
}

export default {
  state,
  actions,
  mutations,
  getters
}
