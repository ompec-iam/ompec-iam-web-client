import Vuex from 'vuex'
import employeeModule from '~/store/Employee'
import departmentModule from '~/store/Department'
import resourceModule from '~/store/Resource'
import permissionStatusModule from '~/store/PermissionStatus'
import managementModule from '~/store/Management'

const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      employee: employeeModule,
      department: departmentModule,
      resource: resourceModule,
      permissionStatus: permissionStatusModule,
      management: managementModule
    }
  })
}

export default createStore
