export default function ({ $auth, redirect }) {
  const isUserPresented = !!$auth.user
  if (!isUserPresented) {
    return redirect('/signin')
  }
  const roles = $auth.user.roles.map(r => r.name)
  if (!roles.includes('ROLE_ADMIN')) {
    return redirect('/employee')
  }
}
