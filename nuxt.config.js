import colors from 'vuetify/es5/util/colors'
import i18n from './config/i18n'
require('dotenv').config()

const envFileName = '.env'

export default {

  mode: 'universal',

  target: 'server',

  head: {
    titleTemplate: '%s',
    title: process.env.SITE_NAME || 'Ompec IAM',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      { name: 'msapplication-TileColor', content: 'white' },
      { name: 'theme-color', content: 'white' }
    ],
    script: [
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/icon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Exo+2:300,400' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' }
    ]
  },

  css: [
    '~/assets/toast.css'
  ],

  plugins: [
    { src: '~/plugins/persistedState.client.js', ssr: false },
    { src: '~/plugins/axios.client.js', ssr: false }
  ],

  components: true,
  loading: {
    color: 'blue',
    height: '2px',
    continuous: true
  },

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/router',
    ['@nuxtjs/dotenv', { filename: envFileName }],
    [
      'nuxt-i18n',
      {
        defaultLocale: 'ru',
        locales: [
          {
            code: 'en',
            name: 'English'
          },
          {
            code: 'ru',
            name: 'Русский'
          }
        ],
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'lang',
          alwaysRedirect: true
        },
        vueI18n: i18n,
        vuex: {
          moduleName: 'i18n',
          syncLocale: true,
          mutations: {
            setLocale: 'I18N_SET_LOCALE',
            setMessages: false
          },
          preserveState: false
        },
        onLanguageSwitched: (previous, current) => {
          if (process.client) {
            const DATE = new Date()
            DATE.setTime(DATE.getTime() + 365 * 24 * 3600 * 1000)
            document.cookie = 'lang=' + current + '; path=/; expires=' + DATE.toUTCString()
          }
        }
      }
    ]
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    [
      'nuxt-mq',
      {
        defaultBreakpoint: 'desktop',
        breakpoints: {
          mobile: 960,
          desktop: Infinity
        }
      }
    ]
  ],

  toast: {
    position: 'bottom-left'
  },

  router: {
    middleware: ['auth']
  },

  axios: {
    baseURL: process.env.API_URL,
    credentials: true,
    headers: {
      common: {
        'Content-Type': 'application/json'
      }
    }
  },

  auth: {
    redirect: {
      login: '/signin',
      logout: '/signin',
      callback: '/signin',
      home: '/employee'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/auth/login',
            method: 'post',
            propertyName: 'accessToken'
          },
          logout: {
            url: '/auth/logout',
            method: 'post'
          },
          user: {
            url: '/user/me',
            method: 'get',
            propertyName: false
          }
        },
        token: {
          property: 'accessToken',
          required: true,
          type: 'Bearer'
        },
        autoFetchUser: true,
        resetOnError: true,
        watchLoggedIn: true
      }
    },
    scopeKey: 'roles'
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.lightBlue.accent4,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  build: {
  }
}
